#include <iostream>
#include "Terrain.h"
#include "GLHelper.h"

using namespace hmk;

bool Terrain::Load(std::string heightMap)
{
	Magick::Blob blob;
	try {
		Magick::Image *img = new Magick::Image(heightMap);
		img->write(&blob, "GRAY");
		width = img->rows();
		height = img->columns();
	} catch (Magick::Error &e)
	{
		std::cerr << ERROR << "Could not load terrain" << std::endl;
		return false;
	}

	data = (unsigned char*)blob.data();
	lenght = blob.length();

	return true;
}

void Terrain::Render()
{

}

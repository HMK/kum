#pragma once

#include <string>
#include "Magick++.h"

namespace hmk
{
	class Terrain
	{
	public:
		Terrain() {}
		~Terrain() {}

		bool Load(std::string heightMap);
		void Render();
	private:
		size_t width, height, lenght;
		unsigned char *data;
	};
}